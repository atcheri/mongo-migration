import {
  convertToHtml,
  buildTitles,
  wrapWithHtmlElement,
  markdowntoHtml,
} from '../helpers';
import { s22 } from '../fixtures/dit';

describe('helpers', () => {
  describe('wrapWithHtmlElement', () => {
    const titles = buildTitles('s22');
    it('wraps the title with h1 tag', () => {
      const res = wrapWithHtmlElement(titles)(['title', s22.title]);
      expect(res).toEqual('<h1>Fraktur des Sternums</h1>');
    });
    it('wraps the abstract with p tag and adds a h3 header with "Kurztext"', () => {
      const res = wrapWithHtmlElement(titles)(['abstract', s22.abstract]);
      expect(res).toEqual(
        '<h3>Kurztext</h3><p>Eine Sternumfraktur entsteht durch eine übermäßige Krafteinwirkung auf den Thoraxbereich. Die häufigste Ursache sind Autounfälle. Man unterscheidet zwischen Querfraktur und Abrissfraktur. Klinisch zeigt sich eine Sternumfraktur durch atemabhängigen Schmerz, Hämatom oder Druckschmerz im Brustbein. Nach adäquater Behandlung heilt eine Sternumfraktur folgenlos aus. Abhängig von der ausgeübten Tätigkeit ist mit einer fiktiven Arbeitsunfähigkeit von 4-16 Wochen zu rechnen.</p>',
      );
    });
    it('wraps the abstract with p tag and adds a h3 header with "Definition"', () => {
      const res = wrapWithHtmlElement(titles)(['description', s22.description]);
      expect(res).toEqual(
        '<h3>Definition</h3><p>Die häufigste Ursache für die Sternumfraktur ist eine direkte, frontale Gewalteinwirkung auf das Brustbein (z.B. Anpralltrauma am Lenkrad). Es handelt sich meistens um eine Querfraktur, seltener um eine Abrissfraktur. Da komplexe bis lebensbedrohliche Begleiterscheinungen auftreten können, werden die Geschädigten stationär aufgenommen. Eine Fraktur des Sternums kommt häufig bei Auffahrunfällen vor. Man unterscheidet zwischen Querfraktur und Abrissfraktur.</p>',
      );
    });
    it('wraps the symptoms with ul tag and adds a h3 header with "Krankheitssymptome"', () => {
      const res = wrapWithHtmlElement(titles)(['symptoms', s22.symptoms]);
      expect(res).toEqual(
        `<h3>Krankheitssymptome</h3><ul><li>Schmerzen</li><li>Druckschmerz</li><li>Atemabhängiger Schmerz</li><li>Hämatom</li></ul>`,
      );
    });
    it('wraps the diagnostics with ul tag and adds a h3 header with "Diagnostik"', () => {
      const res = wrapWithHtmlElement(titles)(['diagnostics', s22.diagnostics]);
      expect(res).toEqual(
        `<h3>Diagnostik</h3><ul><li>Inspektion: Prellmarken, Vorwölbung am Thorax</li><li>Palpation: Druckschmerz, Krepitation</li><li>Auskultation: abgeschwächtes Atemgeräusch</li><li>Röntgen des Thorax</li><li>Ggf. Computertomographie, Sonographie, EKG: um Begleitverletzungen wie z.B. eine Contusio cordis (Prellung des Herzens) oder einen Perikarderguss auszuschließen.</li><li>Bei Verdacht auf Gefäßverletzung -> Angiographie</li><li>Bei Verdacht auf Bronchusruptur -> Bronchoskopie</li></ul>`,
      );
    });
    it('wraps the classification with p tag and adds a h3 header with ""', () => {
      const res = wrapWithHtmlElement(titles)([
        'classification',
        s22.classification,
      ]);
      expect(res).toEqual(
        `<h3>Einteilung</h3><p>Eine Einteilung ist bei einer Sternumfraktur unüblich.</p>`,
      );
    });
    it('wraps the complications with p tag and adds a h3 header with ""', () => {
      const res = wrapWithHtmlElement(titles)([
        'complications',
        s22.complications,
      ]);
      expect(res).toEqual(
        `<h3>Prognose</h3><p>Eine Fraktur des Sternums heilt in der Regel komplikationslos aus. Möglich sind typische OP-assoziierte Komplikationen wie: Infektion Nervenverletzung Thrombose Treten Komplikationen auf, kann der Heilverlauf negativ beeinflusst werden.</p>`,
      );
    });
  });
  describe('convertToHtml', () => {
    it('transforms a dit object into a html entity', () => {
      const html = convertToHtml(s22);

      expect(html).toContain(`<h1>Fraktur des Sternums</h1>`);
      expect(html).toContain(
        `<h3>Definition</h3><p>Die häufigste Ursache für die Sternumfraktur ist eine direkte, frontale Gewalteinwirkung auf das Brustbein (z.B. Anpralltrauma am Lenkrad). Es handelt sich meistens um eine Querfraktur, seltener um eine Abrissfraktur. Da komplexe bis lebensbedrohliche Begleiterscheinungen auftreten können, werden die Geschädigten stationär aufgenommen. Eine Fraktur des Sternums kommt häufig bei Auffahrunfällen vor. Man unterscheidet zwischen Querfraktur und Abrissfraktur.</p>`,
      );
      expect(html).toContain(
        `<h3>Krankheitssymptome</h3><ul><li>Schmerzen</li><li>Druckschmerz</li><li>Atemabhängiger Schmerz</li><li>Hämatom</li></ul>`,
      );
      expect(html).toContain(
        `<h3>Diagnostik</h3><ul><li>Inspektion: Prellmarken, Vorwölbung am Thorax</li><li>Palpation: Druckschmerz, Krepitation</li><li>Auskultation: abgeschwächtes Atemgeräusch</li><li>Röntgen des Thorax</li><li>Ggf. Computertomographie, Sonographie, EKG: um Begleitverletzungen wie z.B. eine Contusio cordis (Prellung des Herzens) oder einen Perikarderguss auszuschließen.</li><li>Bei Verdacht auf Gefäßverletzung -> Angiographie</li><li>Bei Verdacht auf Bronchusruptur -> Bronchoskopie</li></ul>`,
      );
      expect(html).toContain(
        `<h3>Einteilung</h3><p>Eine Einteilung ist bei einer Sternumfraktur unüblich.</p><h3>Therapie</h3><ul><li>Wenn es sich um eine offene Fraktur handelt oder eine starke Dislokation vorliegt, ist das eine Indikation für eine Operation. Liegen diese Faktoren nicht vor, wird die Sternumfraktur konservativ behandelt.</li></ul>`,
      );
      expect(html).toContain(
        `<h3>Prognose</h3><p>Eine Fraktur des Sternums heilt in der Regel komplikationslos aus. Möglich sind typische OP-assoziierte Komplikationen wie: Infektion Nervenverletzung Thrombose Treten Komplikationen auf, kann der Heilverlauf negativ beeinflusst werden.</p>`,
      );
      expect(html).toContain(
        `<h3>Behandlungsprobleme</h3><ul><li>Die Bildung einer Pseudoarthrose ist bei einer Sternumfraktur sehr selten.</li></ul>`,
      );
      expect(html).toContain(
        `<h3>Interventionspunkte</h3><ul><li>6 Wochen</li><li>12 Wochen (ggf. Metallentfernung)</li><li>Beim Auftreten von Komplikationen, die den Heilverlauf verlängern könnten.</li></ul>`,
      );
      expect(html).toContain(
        `<h3>Involvierte Fachbereiche</h3><ul><li>Orthopädie und Unfallchirurgie</li><li>Allgemeinmedizin</li><li>Physiotherapeut</li></ul>`,
      );
      expect(html).toContain(
        `<h3>Angemessene AU</h3><ul><li>Bei leichter Arbeit ca. 4-6 Wochen</li><li>Bei mittelschwerer Arbeit ca. 6-12 Wochen</li><li>Bei schwerer körperlicher Arbeit ca. 8-16 Wochen</li></ul>`,
      );
      expect(html).toContain(
        `<h3>Zu beachtende Vorschädigung</h3><ul><li>Angeborene Deformierungen des Sternums</li><li>Trichterbrust (Pectus excavatum sive infundibulum)</li><li>Kielbrust (Pectus carinatum)</li><li>Kombinationsformen</li></ul>`,
      );
      expect(html).toContain(
        `<h3>Quellenverweis</h3><ul><li>1</li><li>5</li><li>10</li><li>22</li><li>31</li><li>40</li><li>54</li></ul>`,
      );
      expect(html).toContain(
        `<h3>Kurztext</h3><p>Eine Sternumfraktur entsteht durch eine übermäßige Krafteinwirkung auf den Thoraxbereich. Die häufigste Ursache sind Autounfälle. Man unterscheidet zwischen Querfraktur und Abrissfraktur. Klinisch zeigt sich eine Sternumfraktur durch atemabhängigen Schmerz, Hämatom oder Druckschmerz im Brustbein. Nach adäquater Behandlung heilt eine Sternumfraktur folgenlos aus. Abhängig von der ausgeübten Tätigkeit ist mit einer fiktiven Arbeitsunfähigkeit von 4-16 Wochen zu rechnen.</p>`,
      );
      expect(html).toContain(
        `<h3>Häufige Kombinationen mit S22.2</h3><ul><li>R52 0 = Akuter Schmerz</li><li>S20.2 = Prellung des Thorax</li><li>S23.4 = Verstauchung und Zerrung der Rippen und des Sternums</li></ul>`,
      );
      expect(html).toContain(
        `<h3>Synonyme</h3><ul><li>Brustbeinbruch</li><li>Sternumfraktur</li></ul>`,
      );
    });
  });
  describe('markdowntoHtml', () => {
    describe('given an array of markdown', () => {
      const md: string[] = [
        '| Grad | Klinik                                                                  |',
        '| ---- | ----------------------------------------------------------------------- |',
        '| 0    | Keine Nackenbeschwerden, keine körperlichen Symptome                    |',
        '| I    | Nackenbeschwerden, Steifheit oder Spannung, keine körperlichen Symptome |',
        '| II   | Nackenbeschwerden plus muskuloskelettale Symptome                       |',
        '| III  | Nackenbeschwerden plus neurologische Symptome                           |',
        '| IV   | Nackenbeschwerden plus Fraktur oder Dislokation                         |',
      ];
      it('creates a html table', () => {
        const html = markdowntoHtml(md);
        expect(html).toMatch(
          `<table>
<thead>
<tr>
<th>Grad</th>
<th>Klinik</th>
</tr>
</thead>
<tbody>
<tr>
<td>0</td>
<td>Keine Nackenbeschwerden, keine körperlichen Symptome</td>
</tr>
<tr>
<td>I</td>
<td>Nackenbeschwerden, Steifheit oder Spannung, keine körperlichen Symptome</td>
</tr>
<tr>
<td>II</td>
<td>Nackenbeschwerden plus muskuloskelettale Symptome</td>
</tr>
<tr>
<td>III</td>
<td>Nackenbeschwerden plus neurologische Symptome</td>
</tr>
<tr>
<td>IV</td>
<td>Nackenbeschwerden plus Fraktur oder Dislokation</td>
</tr>
</tbody>
</table>`,
        );
      });
    });
  });
});
