import { MongoMemoryServer } from 'mongodb-memory-server';
import { Db, MongoClient, Collection } from 'mongodb';

import { IEncoder, Dit, IIcdRepository } from '../interfaces';
import { s22 } from '../fixtures/dit';
import { updateIcd } from '../migration';

let con: MongoClient;
let db: Db;
let mongoServer: MongoMemoryServer;
let collection: Collection;

beforeAll(async () => {
  mongoServer = new MongoMemoryServer();
  const mongoUri = await mongoServer.getConnectionString();
  con = await MongoClient.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  db = con.db(await mongoServer.getDbName());
});

afterAll(async () => {
  if (con) con.close();
  if (mongoServer) await mongoServer.stop();
});

describe('updateIcd', () => {
  describe('given a mock icd repository with mongodb-memory-server and a mock encoder', () => {
    const mockICdRepo: IIcdRepository = {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      find: (filter: any): Promise<Dit[]> => {
        return collection.find(filter).toArray();
      },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      updateMany: async (filter: any, data: any): Promise<boolean> => {
        const ope = await collection.updateMany(filter, data);
        return ope.result.ok === 1;
      },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      insertOne: async (docs: any): Promise<boolean> => {
        const ope = await collection.insertOne(docs);
        return ope.result.ok === 1;
      },
    };
    const mockEncoder: IEncoder = {
      encode: (str: string): string => {
        return str;
      },
    };
    it('this is a pure test', async () => {
      collection = db.collection('icd');
      await collection.insertOne(s22);
      const code = 'S22.2';
      const dits = await collection.find({ icd: code }).toArray();
      await updateIcd(mockICdRepo, mockEncoder)(dits[0]);
      expect(await collection.countDocuments({ icd: code })).toBe(2);
      expect(
        await collection.countDocuments({ icd: code, status: false }),
      ).toBe(1);
      expect(await collection.countDocuments({ icd: code, status: true })).toBe(
        1,
      );
    });
  });
});
