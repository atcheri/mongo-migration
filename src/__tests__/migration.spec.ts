/* eslint-disable @typescript-eslint/no-unused-vars */
import { updateIcds } from '../migration';
import { Dit, IIcdRepository, IEncoder } from '../interfaces';
import { s22 } from '../fixtures/dit';

describe('updateIcds()', () => {
  describe('given a mock icd repository and a mock encoder', () => {
    const mockICdRepo: IIcdRepository = {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      find: (filter: any): Promise<Dit[]> => {
        return Promise.resolve([s22]);
      },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      updateMany: (filter: any, data: any): Promise<boolean> => {
        return Promise.resolve(true);
      },
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      insertOne: (docs: any): Promise<boolean> => {
        return Promise.resolve(true);
      },
    };
    const mockEncoder: IEncoder = {
      encode: (str: string): string => {
        return str;
      },
    };
    describe('for a collection with 1 dit', () => {
      it('calls the find updateMany insertOne methods once each', async () => {
        const spyFind = jest.fn(() => Promise.resolve([s22]));
        mockICdRepo.find = spyFind;
        const spyUpdateMany = jest.fn(() => Promise.resolve(true));
        mockICdRepo.updateMany = spyUpdateMany;
        const spyInsert = jest.fn(() => Promise.resolve(true));
        mockICdRepo.insertOne = spyInsert;
        await updateIcds(mockICdRepo, mockEncoder);
        expect(spyFind).toHaveBeenCalledTimes(1);
        expect(spyUpdateMany).toHaveBeenCalledTimes(1);
        expect(spyInsert).toHaveBeenCalledTimes(1);
      });
    });

    describe('for a collection with 3 dit', () => {
      it('calls the find once and updateMany/insertOne method 3 times', async () => {
        const spyFind = jest.fn(() => Promise.resolve([s22, s22, s22]));
        mockICdRepo.find = spyFind;
        const spyUpdateMany = jest.fn(() => Promise.resolve(true));
        mockICdRepo.updateMany = spyUpdateMany;
        const spyInsert = jest.fn(() => Promise.resolve(true));
        mockICdRepo.insertOne = spyInsert;
        await updateIcds(mockICdRepo, mockEncoder);
        expect(spyFind).toHaveBeenCalledTimes(1);
        expect(spyUpdateMany).toHaveBeenCalledTimes(3);
        expect(spyInsert).toHaveBeenCalledTimes(3);
      });
    });
  });
});
