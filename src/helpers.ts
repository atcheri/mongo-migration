import showdown from 'showdown';

import { Dit } from './interfaces';

const {
  MONGO_CONNECTION_URL,
  MONG_PORT,
  MONGO_DB_NAME,
  MONGODB_USERNAME,
  MONGODB_PASSWORD,
} = process.env;

const WHITE_SPACE = ' ';

interface ITitles {
  [key: string]: string;
}

export const buildTitles = (code: string): ITitles => {
  return {
    description: 'Definition',
    symptoms: 'Krankheitssymptome',
    diagnostics: 'Diagnostik',
    classification: 'Einteilung',
    therapy: 'Therapie',
    complications: 'Prognose',
    problems_in_therapy: 'Behandlungsprobleme',
    follow_up_care: 'Nachbehandlung',
    intervention_points: 'Interventionspunkte',
    involved_disciplines: 'Involvierte Fachbereiche',
    adequate_sick_leave_period: 'Angemessene AU',
    relevant_pre_injuries: 'Zu beachtende Vorschädigung',
    information_sources: 'Quellenverweis',
    abstract: 'Kurztext',
    combinations: `Häufige Kombinationen mit ${code}`,
    synonyms: 'Synonyme',
  };
};

export const createUri = (): string => {
  return (
    `mongodb://${MONGODB_USERNAME}:${MONGODB_PASSWORD}@${MONGO_CONNECTION_URL}:${MONG_PORT}/${MONGO_DB_NAME}` ||
    'mongodb://db_username:db_password@localhost:27117/db_name'
  );
};

export const convertToHtml = (data: Dit): string => {
  const titles = buildTitles(data.icd);
  const mapped = Object.entries(data)
    .filter(keepHtmlBlocks)
    .sort(sortByPositionWithTitles(titles))
    .map(wrapWithHtmlElement(titles));

  return mapped.join('');
};

export const wrapWithHtmlElement = (titles: ITitles) => ([key, value]: [
  string,
  string | string[] | { text: string[]; table: string[] },
]): string => {
  if (key == 'title') {
    return `<h1>${value}</h1>`;
  }
  const title = titles[key];
  let ret = '';
  ret += `<h3>${title}</h3>`;
  if (Array.isArray(value)) {
    switch (key) {
      case 'symptoms':
      case 'diagnostics':
      case 'therapy':
      case 'problems_in_therapy':
      case 'intervention_points':
      case 'involved_disciplines':
      case 'adequate_sick_leave_period':
      case 'relevant_pre_injuries':
      case 'information_sources':
      case 'combinations':
      case 'synonyms':
        ret += `<ul>${value.map((li) => `<li>${li}</li>`).join('')}</ul>`;
        break;
      default:
        ret += `<p>${value.join(WHITE_SPACE)}</p>`;
    }
    return ret;
  }
  if (typeof value == 'object') {
    switch (key) {
      case 'classification':
      case 'complications':
        ret += `<p>${value.text.join(WHITE_SPACE)}</p>${markdowntoHtml(
          value.table,
        )}`;
    }
    return ret;
  }
  return value;
};

const equalsTitle = (t: string) => (title: string) => t === title;

const sortByPositionWithTitles = (titles: ITitles) => (
  [k1]: [string, string],
  [k2]: [string, string],
): number => {
  const i1 = Object.keys(titles).findIndex(equalsTitle(k1));
  const i2 = Object.keys(titles).findIndex(equalsTitle(k2));
  if (i1 > i2) {
    return 1;
  }
  return -1;
};

const keepHtmlBlocks = ([k]: [string, string]): boolean => {
  return (
    [
      'title',
      'abstract',
      'description',
      'symptoms',
      'diagnostics',
      'classification',
      'therapy',
      'complications',
      'problems_in_therapy',
      'intervention_points',
      'involved_disciplines',
      'adequate_sick_leave_period',
      'relevant_pre_injuries',
      'information_sources',
      'combinations',
      'synonyms',
    ].findIndex((i) => i === k) !== -1
  );
};

export const markdowntoHtml = (md: string[]): string => {
  if (markdowntoHtml.length === 0) {
    return '';
  }
  const converter = new showdown.Converter({ tables: true });
  return converter.makeHtml(md.join('\n'));
};
