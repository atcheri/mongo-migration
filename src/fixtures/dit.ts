import { Dit } from '../interfaces';

export const s22: Dit = {
  icd_group: 'S22',
  icd: 'S22.2',
  icds: ['S22.2'],
  title: 'Fraktur des Sternums',
  dimditext: 'Fraktur des Sternums',
  synonyms: ['Brustbeinbruch', 'Sternumfraktur'],
  abstract: [
    'Eine Sternumfraktur entsteht durch eine übermäßige Krafteinwirkung auf den Thoraxbereich. Die häufigste Ursache sind Autounfälle. Man unterscheidet zwischen Querfraktur und Abrissfraktur. Klinisch zeigt sich eine Sternumfraktur durch atemabhängigen Schmerz, Hämatom oder Druckschmerz im Brustbein. Nach adäquater Behandlung heilt eine Sternumfraktur folgenlos aus.',
    'Abhängig von der ausgeübten Tätigkeit ist mit einer fiktiven Arbeitsunfähigkeit von 4-16 Wochen zu rechnen.',
  ],
  description: [
    'Die häufigste Ursache für die Sternumfraktur ist eine direkte, frontale Gewalteinwirkung auf das Brustbein (z.B. Anpralltrauma am Lenkrad). Es handelt sich meistens um eine Querfraktur, seltener um eine Abrissfraktur.',
    'Da komplexe bis lebensbedrohliche Begleiterscheinungen auftreten können, werden die Geschädigten stationär aufgenommen.',
    'Eine Fraktur des Sternums kommt häufig bei Auffahrunfällen vor. Man unterscheidet zwischen Querfraktur und Abrissfraktur.',
  ],
  symptoms: ['Schmerzen', 'Druckschmerz', 'Atemabhängiger Schmerz', 'Hämatom'],
  diagnostics: [
    'Inspektion: Prellmarken, Vorwölbung am Thorax',
    'Palpation: Druckschmerz, Krepitation',
    'Auskultation: abgeschwächtes Atemgeräusch',
    'Röntgen des Thorax',
    'Ggf. Computertomographie, Sonographie, EKG: um Begleitverletzungen wie z.B. eine Contusio cordis (Prellung des Herzens) oder einen Perikarderguss auszuschließen.',
    'Bei Verdacht auf Gefäßverletzung -> Angiographie',
    'Bei Verdacht auf Bronchusruptur -> Bronchoskopie',
  ],
  classification: {
    text: ['Eine Einteilung ist bei einer Sternumfraktur unüblich.'],
    table: [],
  },
  therapy: [
    'Wenn es sich um eine offene Fraktur handelt oder eine starke Dislokation vorliegt, ist das eine Indikation für eine Operation. Liegen diese Faktoren nicht vor, wird die Sternumfraktur konservativ behandelt.',
  ],
  combinations: [
    'R52 0 = Akuter Schmerz',
    'S20.2 = Prellung des Thorax',
    'S23.4 = Verstauchung und Zerrung der Rippen und des Sternums',
  ],
  problems_in_therapy: [
    'Die Bildung einer Pseudoarthrose ist bei einer Sternumfraktur sehr selten.',
  ],
  follow_up_care: [
    'Bei geschlossener Fraktur ohne Dislokation oder Begleitverletzungen ist zur Vermeidung einer Pneumonie eine konservative Behandlung mit Atemtherapie und Mobilisation erforderlich. Eine Wiedereingliederung in das Berufsleben ist nach acht Wochen möglich.',
    'Bei der operativen Versorgung einer Sternumfraktur ist das erste Therapieziel eine Aktivierung des Patienten. Während der Schmerztherapie ist auch eine Atemtherapie notwendig. Im weiteren Verlauf werden die Drainagesysteme entfernt. Bis zum 3. Tag nach der Operation sollte eine Röntgenkontrolle durchgeführt werden.',
    'Ab der 10. Woche sind Bewegung und Belastung unter Alltagsbedingungen möglich. Bei leichten Tätigkeiten kann die Arbeitsunfähigkeit beendet werden.',
    'Sobald die Fraktur als trainingsstabil eingeschätzt wird, ist bei mittelschweren und schweren Tätigkeiten eine Wiedereingliederung in das Berufsleben möglich. Die Trainingsstabilität wird anhand der klinischen und radiologischen Befunde ermittelt.',
    'Eine Sportfähigkeit besteht nach ca. 6 Monaten.',
  ],
  intervention_points: [
    '6 Wochen',
    '12 Wochen (ggf. Metallentfernung)',
    'Beim Auftreten von Komplikationen, die den Heilverlauf verlängern könnten.',
  ],
  complications: {
    text: [
      'Eine Fraktur des Sternums heilt in der Regel komplikationslos aus.',
      'Möglich sind typische OP-assoziierte Komplikationen wie:',
      'Infektion',
      'Nervenverletzung',
      'Thrombose',
      'Treten Komplikationen auf, kann der Heilverlauf negativ beeinflusst werden.',
    ],
    table: [],
  },
  relevant_pre_injuries: [
    'Angeborene Deformierungen des Sternums',
    'Trichterbrust (Pectus excavatum sive infundibulum)',
    'Kielbrust (Pectus carinatum)',
    'Kombinationsformen',
  ],
  involved_disciplines: [
    'Orthopädie und Unfallchirurgie',
    'Allgemeinmedizin',
    'Physiotherapeut',
  ],
  adequate_sick_leave_period: [
    'Bei leichter Arbeit ca. 4-6 Wochen',
    'Bei mittelschwerer Arbeit ca. 6-12 Wochen',
    'Bei schwerer körperlicher Arbeit ca. 8-16 Wochen',
  ],
  images: ['S5'],
  information_sources: [1, 5, 10, 22, 31, 40, 54],
  Active: true,
  icds_merge: 'S22.2',
  concatenated: 'S22Fraktur des SternumsFraktur des SternumsS22.2',
};
