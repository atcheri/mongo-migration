import { IIcdRepository, IEncoder, Dit } from './interfaces';
import { convertToHtml } from './helpers';

export const updateIcds = async (
  collection: IIcdRepository,
  encoder: IEncoder,
): Promise<number> => {
  try {
    const icds = await collection.find({});
    const convertUpdateAndInsert = async (icds: Dit[]): Promise<number> => {
      const promisses = icds.map(updateIcd(collection, encoder));
      return (await Promise.all(promisses)).length;
    };
    return convertUpdateAndInsert(icds);
  } catch (e) {
    console.error(e);
    return 0;
  }
};

export const updateIcd = (
  collection: IIcdRepository,
  encoder: IEncoder,
) => async (dit: Dit): Promise<null> => {
  const html = convertToHtml(dit);
  delete dit._id;
  await collection.updateMany({ icd: dit.icd }, { $set: { status: false } });
  await collection.insertOne({
    ...dit,
    html: encoder.encode(html),
    status: true,
  });
  return Promise.resolve(null);
};
