export interface Dit {
  _id?: string;
  icd_group: string;
  icd: string;
  icds: string[];
  title: string;
  dimditext: string;
  synonyms: string[];
  abstract: string[];
  description: string[];
  symptoms: string[];
  diagnostics: string[];
  classification: {
    text: string[];
    table: string[];
  };
  therapy: string[];
  combinations: string[];
  problems_in_therapy: string[];
  follow_up_care: string[];
  intervention_points: string[];
  complications: {
    text: string[];
    table: string[];
  };
  relevant_pre_injuries: string[];
  involved_disciplines: string[];
  adequate_sick_leave_period: string[];
  images: string[];
  information_sources: number[];
  Active: boolean;
  icds_merge: string;
  concatenated: string;
}

export interface IEncoder {
  encode: (str: string) => string;
}

export interface IIcdRepository {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  find: (filter: any) => Promise<Dit[]>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  updateMany: (filter: any, data: any) => Promise<boolean>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  insertOne: (docs: any) => Promise<boolean>;
}
