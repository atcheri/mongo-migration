import * as dotenv from 'dotenv';
import mongodb, { Collection, Db } from 'mongodb';
import { AllHtmlEntities } from 'html-entities';

dotenv.config();

import { createUri } from './helpers';
import { IIcdRepository, Dit } from './interfaces';
import { updateIcds } from './migration';

const uri = createUri();

class MongoIcdRepo implements IIcdRepository {
  db: Db;
  collection: Collection;
  constructor(db: Db) {
    this.db = db;
    this.collection = db.collection('icds');
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  find = (filter: any = {}): Promise<Dit[]> => {
    return this.collection.find(filter).toArray();
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  updateMany = async (filter: any, data: any): Promise<boolean> => {
    const ope = await this.collection.updateMany(filter, data);
    return ope.result.ok === 1;
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  insertOne = async (docs: any): Promise<boolean> => {
    const ope = await this.collection.insertOne(docs);
    return ope.result.ok === 1;
  };
}

mongodb.MongoClient.connect(uri, async (err, client) => {
  if (err) {
    return;
  }
  const db = client.db(process.env.MONGO_DB_NAME);
  const repo = new MongoIcdRepo(db);
  const encoder = new AllHtmlEntities();
  const operations = await updateIcds(repo, encoder);
  console.log('operations:', operations);
  client.close();

  process.exit(0);
});
